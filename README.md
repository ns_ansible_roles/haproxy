Роль для установки Haproxy в ОС Ubuntu

Добавляем роль в requirements.yml

```
- name: haproxy
  src: git+https://gitlab.com/ns_ansible_roles/haproxy
  version: main
```
Устанавливаем роль

```
ansible-galaxy install -r requirements.yml
```

Пример плейбука:

```
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - haproxy
```
